package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.model.Flower;

import java.util.Comparator;
import java.util.List;

public class Main {

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            return;
        }

        String xmlFileName = args[0];
        System.out.println("Input ==> " + xmlFileName);

        ////////////////////////////////////////////////////////
        // DOM
        ////////////////////////////////////////////////////////

        // get container
        DOMController domController = new DOMController(xmlFileName);
        List<Flower> flowers = domController.getFlowers();

        // sort (case 1)
        flowers.sort(Comparator.comparing(Flower::getName));

        // save
        String outputXmlFile = "output.dom.xml";
        XMLParser xmlParser = new XMLParser(outputXmlFile);
        xmlParser.parseToXML(flowers);

        ////////////////////////////////////////////////////////
        // SAX
        ////////////////////////////////////////////////////////

        // get
        SAXController saxController = new SAXController(xmlFileName);
        flowers = saxController.getFlowers();
        System.out.println(flowers);
        // sort  (case 2)
        flowers.sort(Comparator.comparing(Flower::getWatering));

        // save
        outputXmlFile = "output.sax.xml";
        xmlParser = new XMLParser(outputXmlFile);
        xmlParser.parseToXML(flowers);

        ////////////////////////////////////////////////////////
        // StAX
        ////////////////////////////////////////////////////////

        // get
        STAXController staxController = new STAXController(xmlFileName);
        flowers = staxController.getFlowers();

        // sort  (case 3)
        flowers.sort(Comparator.comparing(Flower::getAveLenFlower));

        // save
        outputXmlFile = "output.stax.xml";
        xmlParser = new XMLParser(outputXmlFile);
        xmlParser.parseToXML(flowers);
    }

}
