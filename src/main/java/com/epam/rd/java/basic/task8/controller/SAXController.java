package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.model.Flower;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

    private final String xmlFileName;
    private final List<Flower> list = new ArrayList<>();

    public SAXController(String xmlFileName) throws ParserConfigurationException, SAXException, IOException {
        this.xmlFileName = xmlFileName;

        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser = factory.newSAXParser();

        MyXmlHandler myXmlHandler = new MyXmlHandler();
        parser.parse(this.xmlFileName, myXmlHandler);


    }
    public List<Flower> getFlowers() {
        return list;
    }

    private class MyXmlHandler extends DefaultHandler {
        String currentTag;
        Flower flower;

        @Override
        public void startDocument() throws SAXException {
            super.startDocument();
        }

        @Override
        public void endDocument() throws SAXException {
            super.endDocument();
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes)  {
            currentTag = qName;

            if (qName.equals("name")) {
                flower = new Flower();
            }

            if (qName.equals("lighting")) flower.setLighting(attributes.getValue(0));
        }

        @Override
        public void endElement(String uri, String localName, String qName)  {
            if (qName.equals("name")) {
                list.add(flower);
            }
            currentTag = null;
        }

        @Override
        public void characters(char[] ch, int start, int length)  {

            if (currentTag == null) return;


            String data = new String(ch, start, length).strip();
            switch (currentTag) {
                case "name":
                    flower.setName(data);
                    break;
                case "soil":
                    flower.setSoil(data);
                    break;
                case "origin":
                    flower.setOrigin(data);
                    break;
                case "stemColour":
                    flower.setStemColour(data);
                    break;
                case "leafColour":
                    flower.setLeafColour(data);
                    break;
                case "aveLenFlower":
                    flower.setAveLenFlower(Integer.parseInt(data));
                    break;
                case "tempreture":
                    flower.setTempreture(Integer.parseInt(data));
                    break;
                case "watering":
                    flower.setWatering(Integer.parseInt(data));
                    break;
                case "multiplying":
                    flower.setMultiplying(data);
                    break;
                default:
            }

        }
    }

}