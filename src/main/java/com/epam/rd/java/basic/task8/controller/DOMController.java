package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.model.Flower;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

    private String xmlFileName;

    private DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    private DocumentBuilder documentBuilder;
    private Document document;

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;

        try {
            documentBuilder = dbf.newDocumentBuilder();
            document = documentBuilder.parse(xmlFileName);
            document.getDocumentElement().normalize();

        } catch (SAXException | IOException | ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    public List<Flower> getFlowers() {
        List<Flower> flowers = new ArrayList<>();
        NodeList flowerNodes = document.getDocumentElement().getElementsByTagName("flower");

        for (int i = 0; i < flowerNodes.getLength(); i++) {
            if (flowerNodes.item(i).getNodeType() != Node.ELEMENT_NODE) continue;
            flowers.add(getFlower((Element) flowerNodes.item(i)));

        }

        return flowers;
    }


    public Flower getFlower(Element element) {
        Flower flower = new Flower();
        Element visualParameters = (Element) element.getElementsByTagName("visualParameters").item(0);
        Element growingTips = (Element) element.getElementsByTagName("growingTips").item(0);

        flower.setName(element.getElementsByTagName("name").item(0).getTextContent());
        flower.setSoil(element.getElementsByTagName("soil").item(0).getTextContent());
        flower.setOrigin(element.getElementsByTagName("origin").item(0).getTextContent());
        flower.setMultiplying(element.getElementsByTagName("multiplying").item(0).getTextContent());

        flower.setStemColour(visualParameters.getElementsByTagName("stemColour").item(0).getTextContent());
        flower.setLeafColour(visualParameters.getElementsByTagName("leafColour").item(0).getTextContent());
        flower.setAveLenFlower(Integer.parseInt(visualParameters.getElementsByTagName("aveLenFlower").item(0).getTextContent()));

        flower.setTempreture(Integer.parseInt(growingTips.getElementsByTagName("tempreture").item(0).getTextContent()));
        flower.setLighting(growingTips.getElementsByTagName("lighting").item(0).getAttributes().item(0).getTextContent());
        flower.setWatering(Integer.parseInt(growingTips.getElementsByTagName("watering").item(0).getTextContent()));


        return flower;
    }


}
