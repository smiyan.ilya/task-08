package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.DOMController;
import com.epam.rd.java.basic.task8.controller.SAXController;
import com.epam.rd.java.basic.task8.controller.STAXController;

public class Demo {
	
	public static void main(String[] args) throws Exception {
		Main.main(new String[] { "input.xml" });

	}
	
}