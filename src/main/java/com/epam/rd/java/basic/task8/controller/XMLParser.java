package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.model.Flower;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.util.List;


public class XMLParser {
    private Document document;
    private String outputXmlFile;

    public XMLParser(String outputXmlFile) {
        this.outputXmlFile = outputXmlFile;

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            document = builder.newDocument();

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    public void parseToXML(List<Flower> flowers) throws TransformerException {
        Element root = document.createElement("flowers");
        root.setAttribute("xmlns", "http://www.nure.ua");
        root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        root.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd");
        document.appendChild(root);

        for (Flower flower : flowers) {
            Element flowerElement = document.createElement("flower");

            Element name = document.createElement("name");
            name.setTextContent(flower.getName());
            flowerElement.appendChild(name);

            Element soil = document.createElement("soil");
            soil.setTextContent(flower.getSoil());
            flowerElement.appendChild(soil);

            Element origin = document.createElement("origin");
            origin.setTextContent(flower.getOrigin());
            flowerElement.appendChild(origin);

            //visualParameters
            Element visualParameters = document.createElement("visualParameters");

            Element stemColour = document.createElement("stemColour");
            stemColour.setTextContent(flower.getStemColour());
            Element leafColour = document.createElement("leafColour");
            leafColour.setTextContent(flower.getLeafColour());
            Element aveLenFlower = document.createElement("aveLenFlower");
            aveLenFlower.setAttribute("measure", "cm");
            aveLenFlower.setTextContent(String.valueOf(flower.getAveLenFlower()));

            visualParameters.appendChild(stemColour);
            visualParameters.appendChild(leafColour);
            visualParameters.appendChild(aveLenFlower);

            flowerElement.appendChild(visualParameters);

            //growingTips
            Element growingTips = document.createElement("growingTips");

            Element tempreture = document.createElement("tempreture");
            tempreture.setAttribute("measure", "celcius");
            tempreture.setTextContent(String.valueOf(flower.getTempreture()));
            Element lighting = document.createElement("lighting");
            lighting.setAttribute("lightRequiring", "yes");
            Element watering = document.createElement("watering");
            watering.setAttribute("measure", "mlPerWeek");
            watering.setTextContent(String.valueOf(flower.getWatering()));

            growingTips.appendChild(tempreture);
            growingTips.appendChild(lighting);
            growingTips.appendChild(watering);

            flowerElement.appendChild(growingTips);


            Element multiplying = document.createElement("multiplying");
            multiplying.setTextContent(String.valueOf(flower.getMultiplying()));

            flowerElement.appendChild(multiplying);
            root.appendChild(flowerElement);
        }

        writeXmlToFile(document);
    }

    private void writeXmlToFile(Document document) throws TransformerException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        DOMSource source = new DOMSource(document);
        StreamResult result = new StreamResult(outputXmlFile);

        transformer.transform(source, result);
    }
}
