package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.model.Flower;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

    private String xmlFileName;
    private List<Flower> flowers = new ArrayList<>();
    private Flower flower;

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public List<Flower> getFlowers() throws FileNotFoundException, XMLStreamException {


        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLEventReader reader = xmlInputFactory.createXMLEventReader((new FileInputStream(xmlFileName)));

        while (reader.hasNext()) {
            XMLEvent nextEvent = reader.nextEvent();
            if (nextEvent.isStartElement()) {
                StartElement startElement = nextEvent.asStartElement();
                switch (startElement.getName().getLocalPart()) {
                    case "name":
                        flower = new Flower();
                        nextEvent = reader.nextEvent();
                        flower.setName(nextEvent.asCharacters().getData());
                        break;
                    case "soil":
                        nextEvent = reader.nextEvent();
                        flower.setSoil(nextEvent.asCharacters().getData());
                        break;
                    case "origin":
                        nextEvent = reader.nextEvent();
                        flower.setOrigin(nextEvent.asCharacters().getData());
                        break;
                    case "stemColour":
                        nextEvent = reader.nextEvent();
                        flower.setStemColour(nextEvent.asCharacters().getData());
                        break;
                    case "leafColour":
                        nextEvent = reader.nextEvent();
                        flower.setLeafColour(nextEvent.asCharacters().getData());
                        break;
                    case "aveLenFlower":
                        nextEvent = reader.nextEvent();
                        flower.setAveLenFlower(Integer.parseInt(nextEvent.asCharacters().getData()));
                        break;
                    case "tempreture":
                        nextEvent = reader.nextEvent();
                        flower.setTempreture(Integer.parseInt(nextEvent.asCharacters().getData()));
                        break;
                    case "lighting":
                        Attribute lightRequiring = startElement.getAttributeByName(new QName("lightRequiring"));
                        flower.setLighting(lightRequiring.getValue());
                        break;
                    case "watering":
                        nextEvent = reader.nextEvent();
                        flower.setWatering(Integer.parseInt(nextEvent.asCharacters().getData()));
                        break;
                    case "multiplying":
                        nextEvent = reader.nextEvent();
                        flower.setMultiplying(nextEvent.asCharacters().getData());
                        break;

                }

            }
            if (nextEvent.isEndElement()) {
                EndElement endElement = nextEvent.asEndElement();
                if (endElement.getName().getLocalPart().equals("flower")) {
                    flowers.add(flower);
                }
            }
        }


        return flowers;
    }
}